package ru.chernyavskiy.dto;

import ru.chernyavskiy.models.Author;
import ru.chernyavskiy.models.Book;
import ru.chernyavskiy.models.Genre;

import java.math.BigInteger;

public class BookDTO {
    private BigInteger id;

    private String name;

    private String description;

    private Integer publishingYear;

    private String publishingHouse;

    private Genre genre;

    private Author author;

    public BookDTO() {
    }

    public BookDTO(BigInteger id, String name, String description, Integer publishingYear, String publishingHouse, Genre genre, Author author) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.publishingYear = publishingYear;
        this.publishingHouse = publishingHouse;
        this.genre = genre;
        this.author = author;
    }

    public BookDTO(Book book){
        this.id = book.getId();
        this.name = book.getName();
        this.description = book.getDescription();
        this.publishingYear = book.getPublishingYear();
        this.publishingHouse = book.getPublishingHouse();
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPublishingYear() {
        return publishingYear;
    }

    public void setPublishingYear(Integer publishingYear) {
        this.publishingYear = publishingYear;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

}
