package ru.chernyavskiy.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import ru.chernyavskiy.controllers.ForbiddenException;
import ru.chernyavskiy.models.Author;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;


@Component
public class AuthorDAO {
    private final JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert simpleJdbcInsert;

    @Autowired
    public AuthorDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        // return id when we are adding
        this.simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        simpleJdbcInsert.withTableName("author").usingGeneratedKeyColumns("id");
    }

    public List<Author> index() {
        return jdbcTemplate.query("SELECT * FROM author", new BeanPropertyRowMapper<>(Author.class));
    }

    public Optional<Author> show(BigInteger id) {
        return Optional.ofNullable(jdbcTemplate.query("SELECT * FROM author WHERE id=?", new BeanPropertyRowMapper<>(Author.class), id)
                .stream().findAny().orElse(null));
    }

    public int save(Author author) {
        Integer savedAuthor;
        try {
            savedAuthor = simpleJdbcInsert.executeAndReturnKey(new BeanPropertySqlParameterSource(author)).intValue();
        }catch (DataAccessException ex){
            throw new ForbiddenException("Author dont save");
        }
        return savedAuthor;
    }

    public void update(Author updatedAuthor) {
        try {
            jdbcTemplate.update("UPDATE author SET name = ?, biography = ? WHERE id=?",
                    updatedAuthor.getName(), updatedAuthor.getBiography(), updatedAuthor.getId());
        }catch (DataAccessException ex){
            throw new ForbiddenException(ex.getMessage());
        }
    }

    public void delete(BigInteger id) {
        try {
            jdbcTemplate.update("DELETE FROM author WHERE id=?", id);
        }catch(DataAccessException ex){
            throw new ForbiddenException("Error delete author with id=" + id);
        }
    }
}
