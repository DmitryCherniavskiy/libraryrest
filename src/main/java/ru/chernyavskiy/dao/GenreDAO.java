package ru.chernyavskiy.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import ru.chernyavskiy.controllers.ForbiddenException;
import ru.chernyavskiy.models.Genre;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Component
public class GenreDAO {

    private final JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert simpleJdbcInsert;

    @Autowired
    public GenreDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        simpleJdbcInsert.withTableName("genre").usingGeneratedKeyColumns("id");
    }

    public List<Genre> index() {
        return jdbcTemplate.query("SELECT * FROM genre", new BeanPropertyRowMapper<>(Genre.class));
    }

    public Optional<Genre> show(BigInteger id) {
        return Optional.ofNullable(jdbcTemplate.query("SELECT * FROM genre WHERE id=?", new BeanPropertyRowMapper<>(Genre.class), id)
                .stream().findAny().orElse(null));
    }

    public int save(Genre genre) {
        Integer savedGenre;
        try {
            savedGenre = simpleJdbcInsert.executeAndReturnKey(new BeanPropertySqlParameterSource(genre)).intValue();
        }catch(DataAccessException ex){
            throw new ForbiddenException("Genre dont save");
        }
        return savedGenre;
    }

    public void update(Genre updatedGenre) {
        try {
            jdbcTemplate.update("UPDATE genre SET name = ? WHERE id=?",
                    updatedGenre.getName(), updatedGenre.getId());
        }catch (DataAccessException ex){
            throw new ForbiddenException(ex.getMessage());
        }
    }

    public void delete(BigInteger id) {
        try {
            jdbcTemplate.update("DELETE FROM genre WHERE id=?", id);
        }catch (DataAccessException ex){
            throw new ForbiddenException("Error delete genre with id=" + id);
        }
    }
}
