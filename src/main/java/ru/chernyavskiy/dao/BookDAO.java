package ru.chernyavskiy.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import ru.chernyavskiy.controllers.ForbiddenException;
import ru.chernyavskiy.models.Book;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Component
public class BookDAO {
    private final JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert simpleJdbcInsert;

    @Autowired
    public BookDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        simpleJdbcInsert.withTableName("book").usingGeneratedKeyColumns("id");
    }

    public List<Book> index() {
        return jdbcTemplate.query("SELECT * FROM book", new BeanPropertyRowMapper<>(Book.class));
    }

    public Optional<Book> show(BigInteger id) {
        return Optional.ofNullable(jdbcTemplate.query("SELECT * FROM book WHERE id=?", new BeanPropertyRowMapper<>(Book.class), id)
                .stream().findAny().orElse(null));
    }

    public int save(Book book) {
        Integer savedBook;
        try {
            savedBook = simpleJdbcInsert.executeAndReturnKey(new BeanPropertySqlParameterSource(book)).intValue();
        }catch (DataAccessException ex){
            throw new ForbiddenException("Book dont save");
        }
        return savedBook;
    }

    public void update(Book updatedBook) {
        try {
            jdbcTemplate.update("UPDATE book SET name = ?, description = ?, publishing_year = ?, publishing_house = ?, author_id = ?, genre_id = ? WHERE id=?",
                    updatedBook.getName(), updatedBook.getDescription(), updatedBook.getPublishingYear(), updatedBook.getPublishingHouse(),
                    updatedBook.getAuthorId(), updatedBook.getGenreId(), updatedBook.getId());
        }catch (DataAccessException ex){
            throw new ForbiddenException(ex.getMessage());
        }
    }

    public void delete(BigInteger id) {
        try {
            jdbcTemplate.update("DELETE FROM book WHERE id=?", id);
        }catch (DataAccessException ex){
            throw new ForbiddenException("Error delete book with id=" + id);
        }
    }

    public List<Book> findByGenreId(BigInteger id) {
        return jdbcTemplate.query("SELECT * FROM book WHERE genre_id=?", new BeanPropertyRowMapper<>(Book.class), id);
    }

    public List<Book> findByAuthorId(BigInteger id) {
        return jdbcTemplate.query("SELECT * FROM book WHERE author_id=?", new BeanPropertyRowMapper<>(Book.class), id);
    }
}
