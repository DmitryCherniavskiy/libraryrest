package ru.chernyavskiy.models;

import jakarta.validation.constraints.NotEmpty;
import java.math.BigInteger;


public class Genre {
    private BigInteger id;

    @NotEmpty(message = "Name can't be empty")
    private String name;

    public Genre() {
    }

    public Genre(String name) {
        this.name = name;
    }

    public Genre(BigInteger id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
