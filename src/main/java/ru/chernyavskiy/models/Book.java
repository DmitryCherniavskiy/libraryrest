package ru.chernyavskiy.models;

import ru.chernyavskiy.dto.BookDTO;

import java.math.BigInteger;

public class Book {
    private BigInteger id;

    private String name;

    private String description;

    private Integer publishingYear;

    private String publishingHouse;

    private BigInteger genreId;

    private BigInteger authorId;


    public Book(){

    }

    public Book(String name) {
        this.name = name;
    }

    public Book(BigInteger id, String name, String description, Integer publishingYear, String publishingHouse, BigInteger genreId, BigInteger authorId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.publishingYear = publishingYear;
        this.publishingHouse = publishingHouse;
        this.genreId = genreId;
        this.authorId = authorId;
    }

    public Book(String name, String description, Integer publishingYear, String publishingHouse, BigInteger genreId, BigInteger authorId) {
        this.name = name;
        this.description = description;
        this.publishingYear = publishingYear;
        this.publishingHouse = publishingHouse;
        this.genreId = genreId;
        this.authorId = authorId;
    }

    public Book(BookDTO bookDTO){
        this.id = bookDTO.getId();
        this.name = bookDTO.getName();
        this.description = bookDTO.getDescription();
        this.publishingYear = bookDTO.getPublishingYear();
        this.publishingHouse = bookDTO.getPublishingHouse();
        this.genreId = bookDTO.getGenre().getId();
        this.authorId = bookDTO.getAuthor().getId();
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPublishingYear() {
        return publishingYear;
    }

    public void setPublishingYear(Integer publishingYear) {
        this.publishingYear = publishingYear;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }


    public BigInteger getGenreId() {
        return genreId;
    }

    public void setGenreId(BigInteger genreId) {
        this.genreId = genreId;
    }

    public BigInteger getAuthorId() {
        return authorId;
    }

    public void setAuthorId(BigInteger authorId) {
        this.authorId = authorId;
    }

}
