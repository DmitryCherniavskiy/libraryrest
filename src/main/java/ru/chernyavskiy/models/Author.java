package ru.chernyavskiy.models;

import jakarta.validation.constraints.NotEmpty;
import java.math.BigInteger;
import java.util.Objects;

public class Author {
    private BigInteger id;

    @NotEmpty(message = "Author's name can't be empty")
    private String name;

    private String biography;

    public Author() {
    }

    public Author(String name, String biography) {
        this.name = name;
        this.biography = biography;
    }

    public Author(BigInteger id, String name, String biography) {
        this.id = id;
        this.name = name;
        this.biography = biography;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return Objects.equals(id, author.id) && Objects.equals(name, author.name) && Objects.equals(biography, author.biography);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, biography);
    }
}
