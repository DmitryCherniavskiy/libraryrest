package ru.chernyavskiy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;
import ru.chernyavskiy.config.SpringConfig;
import ru.chernyavskiy.controllers.ForbiddenException;
import ru.chernyavskiy.dao.AuthorDAO;
import ru.chernyavskiy.models.Author;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
@Import(SpringConfig.class)
public class AuthorService {
    private final AuthorDAO authorDAO;

    @Autowired
    public AuthorService(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public List<Author> getAllAuthor(){
        return authorDAO.index();
    }

    public Optional<Author> getAuthorById(BigInteger id){
        return authorDAO.show(id);
    }

    public int save(Author author){
        return authorDAO.save(author);
    }

    public void editById(Author author){
        if (authorDAO.show(author.getId()).isPresent()){
            authorDAO.update(author);
        }else{
            throw new ForbiddenException("Author with id = " + author.getId() + " not found");
        }
    }

    public void deleteById(BigInteger id){
        if (authorDAO.show(id).isPresent()){
            authorDAO.delete(id);
        }else{
            throw new ForbiddenException("Author with id = " + id + " not found");
        }
    }

}
