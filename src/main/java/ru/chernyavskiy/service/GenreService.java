package ru.chernyavskiy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;
import ru.chernyavskiy.config.SpringConfig;
import ru.chernyavskiy.controllers.ForbiddenException;
import ru.chernyavskiy.dao.GenreDAO;
import ru.chernyavskiy.models.Genre;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
@Import(SpringConfig.class)
public class GenreService {
    private final GenreDAO genreDAO;

    @Autowired
    public GenreService(GenreDAO genreDAO) {
        this.genreDAO = genreDAO;
    }

    public List<Genre> getAllGenre(){
        return genreDAO.index();
    }

    public Optional<Genre> getGenreById(BigInteger id){
        return genreDAO.show(id);
    }

    public Integer save(Genre genre){
        return genreDAO.save(genre);
    }

    public void editById(Genre genre){
        if (genreDAO.show(genre.getId()).isPresent()){
            genreDAO.update(genre);
        }else{
            throw new ForbiddenException("Genre with id = " + genre.getId() + " not found");
        }
    }

    public void deleteById(BigInteger id){
        if (genreDAO.show(id).isPresent()){
            genreDAO.delete(id);
        }else{
            throw new ForbiddenException("Genre with id = " + id + " not found");
        }
    }

}
