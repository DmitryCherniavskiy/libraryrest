package ru.chernyavskiy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;
import ru.chernyavskiy.config.SpringConfig;
import ru.chernyavskiy.controllers.ForbiddenException;
import ru.chernyavskiy.dao.AuthorDAO;
import ru.chernyavskiy.dao.BookDAO;
import ru.chernyavskiy.dao.GenreDAO;
import ru.chernyavskiy.dto.BookDTO;
import ru.chernyavskiy.models.Book;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Import(SpringConfig.class)
public class BookService {
    private final BookDAO bookDAO;
    private final GenreDAO genreDAO;
    private final AuthorDAO authorDAO;


    @Autowired
    public BookService(BookDAO bookDAO, GenreDAO genreDAO, AuthorDAO authorDAO) {
        this.bookDAO = bookDAO;
        this.genreDAO = genreDAO;
        this.authorDAO = authorDAO;
    }

    public BookDTO bookMapping(Book book){
        BookDTO bookDTO = new BookDTO(book);
        if (book.getAuthorId() != null && authorDAO.show(book.getAuthorId()).isPresent())
            bookDTO.setAuthor(authorDAO.show(book.getAuthorId()).get());
        if (book.getGenreId() != null && genreDAO.show(book.getGenreId()).isPresent())
            bookDTO.setGenre(genreDAO.show(book.getGenreId()).get());
        return bookDTO;
    }

    public List<BookDTO> getAllBook(){
        List<Book> bookList = bookDAO.index();
        List<BookDTO> bookDTOList = new ArrayList<>();
        bookList.stream().forEach(x -> bookDTOList.add(bookMapping(x)));
        return bookDTOList;
    }

    public BookDTO getBookById(BigInteger id){
        Optional<Book> book = bookDAO.show(id);
        if (book.isPresent()) {
            return bookMapping(book.get());
        }else{
            throw new ForbiddenException("Book with id = " + id + " not found");
        }
    }

    public List<Book> getBookByGenreId(BigInteger id){
        return bookDAO.findByGenreId(id);
    }

    public List<Book> getBookByAuthorId(BigInteger id){
        return bookDAO.findByAuthorId(id);
    }

    public int save(Book book){
        if (authorDAO.show(book.getAuthorId()).isPresent()){
            if (genreDAO.show(book.getGenreId()).isPresent()){
                return bookDAO.save(book);
            }
            throw new ForbiddenException("Genre for this book not found");
        }
        throw new ForbiddenException("Author for this book not found");
    }

    public void editById(Book book){
        if (bookDAO.show(book.getId()).isPresent()){
            bookDAO.update(book);
        }else{
            throw new ForbiddenException("Book with id = " + book.getId() + " not found");
        }
    }

    public void deleteById(BigInteger id){
        if (bookDAO.show(id).isPresent()){
            bookDAO.delete(id);
        }else{
            throw new ForbiddenException("Book with id = " + id + " not found");
        }
    }

}
