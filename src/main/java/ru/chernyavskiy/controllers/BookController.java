package ru.chernyavskiy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.chernyavskiy.dto.BookDTO;
import ru.chernyavskiy.models.Book;
import ru.chernyavskiy.service.BookService;

import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {
    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("")
    public List<BookDTO> index() {
        return bookService.getAllBook();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> show(@PathVariable("id") BigInteger id) {
        BookDTO book = bookService.getBookById(id);
        if(book != null){
            return ResponseEntity.ok(book);
        }
        throw new ForbiddenException("Not found book with id = " + id);
    }


    @RequestMapping(value="" , method= RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<BookDTO> create(@RequestBody Book book) {
        BigInteger savedBookId = BigInteger.valueOf(bookService.save(book));
        return ResponseEntity.ok(bookService.getBookById(savedBookId));
    }

    @RequestMapping(value="" , method= RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<BookDTO> update(@RequestBody Book book) {
        bookService.editById(book);
        return ResponseEntity.ok(bookService.getBookById(book.getId()));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") BigInteger id) {
        bookService.deleteById(id);
        return ResponseEntity.ok("Success delete");
    }

   @GetMapping("/byAuthorId/{authorId}")
    public ResponseEntity<Object> showByAuthor(@PathVariable("authorId") BigInteger id) {
        List<Book> book = bookService.getBookByAuthorId(id);
        if(book != null){
            return ResponseEntity.ok(book);
        }
        throw new ForbiddenException("Not found book with author id = " + id);
    }

    @GetMapping("/byGenreId/{genreId}")
    public ResponseEntity<Object> showByGenre(@PathVariable("genreId") BigInteger id) {
        List<Book> book = bookService.getBookByGenreId(id);
        if(book != null){
            return ResponseEntity.ok(book);
        }
        throw new ForbiddenException("Not found book with genre id = " + id);
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<Object> handleException(ForbiddenException e) {
        return ResponseEntity.ok(e.getMessage());
    }
}
