package ru.chernyavskiy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.chernyavskiy.models.Author;
import ru.chernyavskiy.service.AuthorService;
import ru.chernyavskiy.service.BookService;

import java.math.BigInteger;
import java.util.Optional;


@RestController
@RequestMapping("/authors")
public class AuthorController {
    private final AuthorService authorService;
    private final BookService bookService;

    @Autowired
    public AuthorController(AuthorService authorService, BookService bookService) {
        this.authorService = authorService;
        this.bookService = bookService;
    }

    @GetMapping()
    public ResponseEntity<Object> index() {
        return ResponseEntity.ok(authorService.getAllAuthor());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> show(@PathVariable("id") BigInteger id) {
        Optional<Author> author = authorService.getAuthorById(id);
        if(!author.isPresent()){
            throw new ForbiddenException("Author with id = " + id + " not found");
        }
        return ResponseEntity.ok(author.get());
    }


    @RequestMapping(value="" , method= RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> create(@RequestBody Author author) {
        int savedAuthorId = authorService.save(author);
        Optional<Author> savedAuthor = authorService.getAuthorById(BigInteger.valueOf(savedAuthorId));
        if (!savedAuthor.isPresent()){
            throw new ForbiddenException("Author not create");
        }else{
            return ResponseEntity.ok(savedAuthor.get());
        }
    }


    @RequestMapping(value="" , method= RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> update(@RequestBody Author author) {
        authorService.editById(author);
        return ResponseEntity.ok(authorService.getAuthorById(author.getId()).get());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") BigInteger id) {
        if (bookService.getBookByAuthorId(id).isEmpty()) {
            authorService.deleteById(id);
            return ResponseEntity.ok("Delete success");
        }else{
            throw new ForbiddenException("Unable to delete a author while he has books");
        }
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<Object> handleException(ForbiddenException e) {
        return ResponseEntity.ok(e.getMessage());
    }
}
