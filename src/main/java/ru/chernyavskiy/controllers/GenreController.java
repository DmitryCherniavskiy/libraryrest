package ru.chernyavskiy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.chernyavskiy.models.Genre;
import ru.chernyavskiy.service.BookService;
import ru.chernyavskiy.service.GenreService;

import java.math.BigInteger;
import java.util.Optional;


@RestController
@RequestMapping("/genres")
public class GenreController {
    private final GenreService genreService;
    private final BookService bookService;


    @Autowired
    public GenreController(GenreService genreService, BookService bookService) {
        this.genreService = genreService;
        this.bookService = bookService;
    }

    @GetMapping()
    public ResponseEntity<Object> index() {
        return ResponseEntity.ok(genreService.getAllGenre());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> show(@PathVariable("id") BigInteger id) {
        Optional<Genre> genre = genreService.getGenreById(id);
        if (!genre.isPresent()){
            throw new ForbiddenException("Genre with id = " + id + " not found");
        }
        return ResponseEntity.ok(genre.get());
    }

    @RequestMapping(value="" , method= RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> create(@RequestBody Genre genre) {
        int savedGenreId = genreService.save(genre);
        Optional<Genre> savedGenre = genreService.getGenreById(BigInteger.valueOf(savedGenreId));
        if (!savedGenre.isPresent()){
            throw new ForbiddenException("Genre not create");
        }else{
            return ResponseEntity.ok(savedGenre.get());
        }
    }

    @RequestMapping(value="" , method= RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> update(@RequestBody Genre genre) {
        genreService.editById(genre);
        return ResponseEntity.ok(genreService.getGenreById(genre.getId()).get());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") BigInteger id) {
        if (bookService.getBookByGenreId(id).isEmpty()) {
            genreService.deleteById(id);
            return ResponseEntity.ok("Delete success");
        }else{
            throw new ForbiddenException("Unable to delete a genre while there are books in that genre");
        }
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<Object> handleException(ForbiddenException e) {
        return ResponseEntity.ok(e.getMessage());
    }
}
