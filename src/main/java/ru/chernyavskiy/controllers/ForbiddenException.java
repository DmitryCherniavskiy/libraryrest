
package ru.chernyavskiy.controllers;


public class ForbiddenException extends RuntimeException {
   String message;
   public ForbiddenException(){};

   public ForbiddenException(String message){
       this.message = message;
   }

   public String getMessage(){
       return message;
   }
    
}
